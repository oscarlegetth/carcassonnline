package game;

import gui.GUI;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import client.ClientHandler;
import server.ServerHandler;

/**
 * The GameHandler class handles the tiles that are in play and left to play.
 *
 */
public class GameHandler {
	private Queue<Tile> tilesLeft;
	private ArrayList<Tile> tilesInPlay;
	private ArrayList<Tile> adjacentSquares;

	private boolean change;
	private Tile nextTile;
	private int nextTileRotation;

	private GUI gui;
	private Handler handler;

	private int activePlayer;
	private ArrayList<String> players;
	private ArrayList<Integer> scores;

	/**
	 * Creates a new GameHandler that can handle one (1) game of carcassonne
	 * 
	 * @param serverHandler
	 *            The GUI that the game should be drawn in
	 */
	public GameHandler(Handler handler) {
		tilesLeft = new LinkedList<Tile>();
		tilesInPlay = new ArrayList<Tile>();
		adjacentSquares = new ArrayList<Tile>();
		players = new ArrayList<String>();
		scores = new ArrayList<Integer>();
		activePlayer = 0;
		change = true;
		this.handler = handler;
		addTestTiles();
		addTestPlayer("Test");
	}

	/**
	 * TODO
	 * 
	 * @return
	 */
	public boolean gameOver() {
		return false;
	}

	/**
	 * Adds some tiles to test if the game works
	 */
	public void addTestTiles() {
		addRiver();
		Tile[] testQueue = TestSomeTiles.someQueueTiles();
		testQueue = shuffle(testQueue);
		for (Tile t : testQueue)
			addTileToQueue(t);

	}

	private void addRiver() {
		Tile[] riverStart = TestSomeTiles.riverStart();
		Tile[] river = TestSomeTiles.riverTiles();
		Tile[] riverEnd = TestSomeTiles.riverEnd();
		river = shuffle(river);
		addTileToQueue(riverStart[0]);
		for (Tile t : river)
			addTileToQueue(t);
		addTileToQueue(riverEnd[0]);
	}

	private Tile[] shuffle(Tile[] arr) {
		Random random = new Random();
		int count = arr.length;
		for (int i = count; i > 1; i--) {
			int index1 = i - 1, index2 = random.nextInt(i);
			Tile temp = arr[index1];
			arr[index1] = arr[index2];
			arr[index2] = temp;
		}
		return arr;
	}

	public void addTestPlayer(String name) {
		players.add(name);
		scores.add(0);
	}

	/**
	 * Adds a tile to the queue of tiles that may be drawn
	 * 
	 * @param t
	 *            The tile that should be added
	 */
	public void addTileToQueue(Tile t) {
		tilesLeft.add(t);
	}

	/**
	 * Add a tile to the game's board. Preferably adjacent to another tile.
	 */
	public boolean addTile() {
		Tile t = tilesLeft.peek();
		if (t == null)
			// last tile reached
			return false;
		int rotation = nextTileRotation();
		int coords[] = gui.markedTileCoords();
		int x = coords[0], y = coords[1];
		t.setCoords(x, y, rotation);
		// get the neighbours {east, west, south, north}
		Tile[] neighbours = { getTile(x + 1, y), getTile(x - 1, y),
				getTile(x, y + 1), getTile(x, y - 1) };

		if (neighbours[0] == null && neighbours[1] == null
				&& neighbours[2] == null && neighbours[3] == null
				&& !tilesInPlay.isEmpty())
			// no neighbours (unless it's the first tile)
			return false;

		if (!t.canConnect(neighbours[0], Tile.BORDER_DIR_W)
				|| !t.canConnect(neighbours[1], Tile.BORDER_DIR_E)
				|| !t.canConnect(neighbours[2], Tile.BORDER_DIR_N)
				|| !t.canConnect(neighbours[3], Tile.BORDER_DIR_S))
			// at least on neighbour does not have a matching border
			return false;

		if (t.isRiver() && !tilesInPlay.isEmpty()) {
			boolean riverneighbour = false;
			if (neighbours[0] != null)
				if (neighbours[0].getBorderType(neighbours[0]
						.accountForRotation(Tile.BORDER_DIR_W)) == Tile.BORDER_RIVER)
					riverneighbour = true;
			if (neighbours[1] != null)
				if (neighbours[1].getBorderType(neighbours[1]
						.accountForRotation(Tile.BORDER_DIR_E)) == Tile.BORDER_RIVER)
					riverneighbour = true;
			if (neighbours[2] != null)
				if (neighbours[2].getBorderType(neighbours[2]
						.accountForRotation(Tile.BORDER_DIR_N)) == Tile.BORDER_RIVER)
					riverneighbour = true;
			if (neighbours[3] != null)
				if (neighbours[3].getBorderType(neighbours[3]
						.accountForRotation(Tile.BORDER_DIR_S)) == Tile.BORDER_RIVER)
					riverneighbour = true;

			if (!riverneighbour)
				return false;
		}

		// all is good, make it a neighbour to all neighbours
		if (neighbours[0] != null)
			t.addNeighbour(neighbours[0], Tile.BORDER_DIR_W);
		if (neighbours[1] != null)
			t.addNeighbour(neighbours[1], Tile.BORDER_DIR_E);
		if (neighbours[2] != null)
			t.addNeighbour(neighbours[2], Tile.BORDER_DIR_N);
		if (neighbours[3] != null)
			t.addNeighbour(neighbours[3], Tile.BORDER_DIR_S);

		// update adjacent squares
		for (int i = 0; i < adjacentSquares.size(); ++i) {
			if (adjacentSquares.get(i).collide(t)) {
				adjacentSquares.remove(i);
				break;
			}
		}

		scoreTile(t);

		addAdjacentSquare(x + 1, y);
		addAdjacentSquare(x - 1, y);
		addAdjacentSquare(x, y + 1);
		addAdjacentSquare(x, y - 1);
		tilesLeft.poll();
		nextTile = tilesLeft.peek();
		tilesInPlay.add(t);

		change = true;
		gui.repaint();
		handler.addTile(t);
		return true;
	}

	public void scoreTile(Tile t) {
		boolean[] bordersScored = { false, false, false, false, false, false,
				false, false, false, false, false, false };
		int scoreInc = 0;
		// score all borders in order
		for (int i = 0; i < bordersScored.length; ++i) {
			if (!bordersScored[i]) {
				scoreInc += t.score(Tile.BORDER_DIR_NNW + i);
				// get all borders reachable from this border so we don't score
				// them as well
				int[] reachable = t
						.getReachableBorders(Tile.BORDER_DIR_NNW + i);
				for (int j : reachable) {
					bordersScored[j - Tile.BORDER_DIR_NNW] = true;
				}

				EntryPoint[] ep = t.getAllConnections(Tile.BORDER_DIR_NNW + i);
				for (EntryPoint e : ep) {
					if (t.collide(e.tile)) {
						reachable = t.getReachableBorders(e.border);
						for (int j : reachable) {
							bordersScored[j - Tile.BORDER_DIR_NNW] = true;
						}

					}
				}

			}
		}
		scores.set(activePlayer, scores.get(activePlayer) + scoreInc);
		activePlayer++;
		if (activePlayer >= players.size())
			activePlayer -= players.size();
	}

	/**
	 * Adds an "empty" square to the game's board that shows that a new tiles
	 * may be placed at these coordinates.
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 */
	private void addAdjacentSquare(int x, int y) {
		boolean collide = false;
		for (Tile t : adjacentSquares) {
			if (t.collide(x, y)) {
				collide = true;
				break;
			}
		}

		for (Tile t : tilesInPlay) {
			if (t.collide(x, y)) {
				collide = true;
				break;
			}
		}

		if (!collide) {
			adjacentSquares.add(new Tile(x, y));
		}
	}

	public void resetRotation() {
		nextTileRotation = 0;
	}

	public void rotateLeft() {
		nextTileRotation--;
		if (nextTileRotation == -1)
			nextTileRotation = 3;
		gui.repaint();
	}

	public void rotateRight() {
		nextTileRotation++;
		if (nextTileRotation == 4)
			nextTileRotation = 0;
		gui.repaint();
	}

	public Tile nextTile() {
		if (nextTile == null)
			nextTile = tilesLeft.peek();
		return nextTile;
	}

	/**
	 * Returns all tiles currently in play.
	 * 
	 * @return An array of GameTiles of the tiles in play.
	 */
	public Tile[] tiles() {
		change = false;
		return tilesInPlay.toArray(new Tile[tilesInPlay.size()]);
	}

	/**
	 * Get a tile on a x and y-coordinate
	 * 
	 * @param x
	 *            The tile's x-coordinate
	 * @param y
	 *            The tile's y-coordiante
	 * @return The Tile at (x, y), or null if there is no Tile at those
	 *         coordinates
	 */
	public Tile getTile(int x, int y) {
		for (Tile t : tilesInPlay) {
			if (t.collide(x, y))
				return t;
		}
		return null;
	}

	/**
	 * Returns empty <code>GameTile</code>s that represents squares where tiles
	 * may be placed. <b>The Tile field of these GameTiles is null</b>
	 * 
	 * @return An array of GameTiles that represents squares where new tiles may
	 *         be placed.
	 */
	public Tile[] adjacentSquares() {
		return adjacentSquares.toArray(new Tile[adjacentSquares.size()]);
	}

	/**
	 * Sets this GameHandler's GUI.
	 * 
	 * @param gui
	 *            The new GUI.
	 */

	public void setGUI(GUI gui) {
		this.gui = gui;
	}

	/**
	 * Returns true if a new tile have been added to the board since the last
	 * call of <code>tiles()</code>
	 * 
	 * @return True if a change has been made.
	 */

	public boolean hasChanged() {
		return change;
	}

	public int nextTileRotationImage() {
		return nextTileRotation;
	}

	public int nextTileRotation() {
		switch (nextTileRotation) {
		case 0:
			return Tile.ROTATION_N;
		case 1:
			return Tile.ROTATION_W;
		case 2:
			return Tile.ROTATION_S;
		case 3:
			return Tile.ROTATION_E;
		}
		return -1;
	}

	public String[] getPlayers() {
		return players.toArray(new String[players.size()]);
	}

	public int[] getScores() {
		int[] res = new int[scores.size()];
		for (int i = 0; i < scores.size(); ++i) {
			res[i] = scores.get(i);
		}
		return res;
	}

}
