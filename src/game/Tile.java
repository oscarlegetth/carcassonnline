package game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * A class for representing a tile.
 *
 */
public class Tile {

	public static final int BORDER_EMPTY = -1;

	public static final int BORDER_DIR_NNW = 1;
	public static final int BORDER_DIR_N = 2;
	public static final int BORDER_DIR_NNE = 3;
	public static final int BORDER_DIR_ENE = 4;
	public static final int BORDER_DIR_E = 5;
	public static final int BORDER_DIR_ESE = 6;
	public static final int BORDER_DIR_SSE = 7;
	public static final int BORDER_DIR_S = 8;
	public static final int BORDER_DIR_SSW = 9;
	public static final int BORDER_DIR_WSW = 10;
	public static final int BORDER_DIR_W = 11;
	public static final int BORDER_DIR_WNW = 12;

	public static final int BORDER_FIELD = 20;
	public static final int BORDER_CITY = 21;
	public static final int BORDER_RIVER = 22;
	public static final int BORDER_ROAD = 23;

	public static final int ROTATION_N = 100;
	public static final int ROTATION_E = 101;
	public static final int ROTATION_S = 102;
	public static final int ROTATION_W = 103;

	public static final int SPECIAL_SHIELD = 0;

	private int northBorder;
	private int eastBorder;
	private int southBorder;
	private int westBorder;
	private int special;
	private int specialParent;

	private Tile northTile;
	private Tile eastTile;
	private Tile southTile;
	private Tile westTile;

	private ArrayList<Connection> connections;
	private int image;

	private int x; // this tile's x-coordinate on the board
	private int y; // this tile's y-coordinate on the board
	private int rotation;

	/**
	 * Creates a new tile. The four first arguments are the north, east, south
	 * and west borders' types respectively. These determine what is at the four
	 * borders. The argument special is used if this tile has a special property
	 * like sheep or shields in towns. The specialParent is a border that
	 * explains what the special is connected to. <br>
	 * <b>Examples:</b> <br>
	 * <code>Tile(BORDER_FIELD, BORDER_FIELD, BORDER_FIELD, BORDER_FIELD,
	 * SPECIAL_MONASTERY, BORDER_EMPTY)</code><br>
	 * Creates a tile with fields at all borders and a monastery in the center. <br>
	 * <code>Tile(BORDER_TOWN_W, BORDER_ROAD_S, BORDER_ROAD_E, BORDER_TOWN_N,
	 * SPECIAL_SHIELD, BORDER_TOWN_N)</code><br>
	 * Creates a tile which has a city that connects the north and west border,
	 * and a road that connects the east and south border. The town has a
	 * shield.
	 * 
	 * @param northBorder
	 *            See the public static ints starting with BORDER_
	 * @param eastBorder
	 *            See the public static ints starting with BORDER_
	 * @param southBorder
	 *            See the public static ints starting with BORDER_
	 * @param westBorder
	 *            See the public static ints starting with BORDER_
	 * @param special
	 *            See the public static ints starting with SPECIAL_
	 * @param specialParent
	 *            See the public static ints starting with BORDER_
	 */
	public Tile(int northBorder, int eastBorder, int southBorder,
			int westBorder, int special, int specialParent) {
		this.northBorder = northBorder;
		this.eastBorder = eastBorder;
		this.southBorder = southBorder;
		this.westBorder = westBorder;
		this.special = special;
		this.specialParent = specialParent;
		connections = new ArrayList<Connection>();
	}

	/**
	 * Create an empty tile with only an x and y-coordinate
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 */
	public Tile(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public int getImage() {
		return image;
	}

	/**
	 * Get what's at a certain border.
	 * 
	 * @param border
	 *            The border to query
	 * @return The type of the specified border
	 */

	public int getBorderType(int border) {
		int type = 0;
		switch (border) {
		case BORDER_DIR_NNE:
		case BORDER_DIR_N:
		case BORDER_DIR_NNW:
			type = northBorder;
			break;
		case BORDER_DIR_ENE:
		case BORDER_DIR_E:
		case BORDER_DIR_ESE:
			type = eastBorder;
			break;
		case BORDER_DIR_SSE:
		case BORDER_DIR_S:
		case BORDER_DIR_SSW:
			type = southBorder;
			break;
		case BORDER_DIR_WSW:
		case BORDER_DIR_W:
		case BORDER_DIR_WNW:
			type = westBorder;
			break;
		default:
			return BORDER_EMPTY;
		}
		if (isNWSE(border))
			return type;
		if (type == BORDER_ROAD)
			return BORDER_FIELD;
		else if (type == BORDER_RIVER)
			return BORDER_FIELD;
		return type;

	}

	/**
	 * Checks if a border is of type BORDER_DIR_*.
	 * 
	 * @param border
	 *            The border to check
	 * @return True if the border is a BORDER_DIR_*
	 */

	public boolean isBorder(int border) {
		return border >= BORDER_DIR_NNW && border <= BORDER_DIR_WNW;
	}

	/**
	 * Checks if a border is BORDER_DIR_N, BORDER_DIR_W, BORDER_DIR_S or
	 * BORDER_DIR_W.
	 * 
	 * @param border
	 *            The border to check
	 * @return True if the border is any of the above.
	 */
	public boolean isNWSE(int border) {
		return border == BORDER_DIR_N || border == BORDER_DIR_W
				|| border == BORDER_DIR_S || border == BORDER_DIR_E;
	}

	/**
	 * Checks if this tile can be connected to another tile. The border
	 * parameter is the this tile's border that will border the other tile, the
	 * border is not this tile's representation of border, as the tile may be
	 * rotated, but instead the border seen from the game board.
	 * 
	 * @param t
	 *            The tile to check if it can be connected to this tile
	 * @param border
	 *            This tile's border (not accounting for the tile's rotation)
	 *            that will border the tile.
	 * @return
	 */

	public boolean canConnect(Tile t, int border) {
		if (t == null)
			return true;
		int ourBorder = accountForRotation(otherSide(border));
		int theirBorder = t.accountForRotation(border);
		return getBorderType(ourBorder) == t.getBorderType(theirBorder)
				&& getNeighbour(ourBorder) == null
				&& t.getNeighbour(theirBorder) == null;

	}

	private int otherSide(int border) {
		switch (border) {
		case BORDER_DIR_N:
			return BORDER_DIR_S;
		case BORDER_DIR_S:
			return BORDER_DIR_N;
		case BORDER_DIR_W:
			return BORDER_DIR_E;
		case BORDER_DIR_E:
			return BORDER_DIR_W;
		default:
			return BORDER_EMPTY;
		}
	}

	public int accountForRotation(int border) {
		switch (rotation) {
		case ROTATION_N:
			break;
		case ROTATION_E:
			border += 3;
			break;
		case ROTATION_S:
			border += 6;
			break;
		case ROTATION_W:
			border += 9;
			break;
		default:
			return BORDER_EMPTY;
		}
		if (border > 12)
			border -= 12;
		return border;

	}

	/**
	 * Get all the tiles reachable from a border in this tile.
	 * 
	 * @return
	 */
	public EntryPoint[] getAllConnections(int border) {

		ArrayList<EntryPoint> list = new ArrayList<EntryPoint>();
		Queue<EntryPoint> queue = new LinkedList<EntryPoint>();
		queue.add(new EntryPoint(this, border));
		while (!queue.isEmpty()) {
			EntryPoint check = queue.poll();

			int[] c = check.tile.getReachableBorders(check.border);
			for (int i : c) {
				Tile t = check.tile.getNeighbour(i);

				int neighboursBorder = check.tile.connectedVia(i);
				// make sure there is a neighbour
				if (t != null && neighboursBorder != BORDER_EMPTY) {

					// construct an entrypoint representing a border on the
					// neighbouring tile that we will check in the future.
					EntryPoint ep = new EntryPoint(t, neighboursBorder);
					boolean add = true;
					for (EntryPoint e : list) {

						// don't add entrypoints that are connected within a
						// tile or entrypoints we have already checked, or are
						// connected to entrypoints we have already checked
						if (e.tile.collide(ep.tile)
								&& e.tile.isReachable(e.border, ep.border)) {
							add = false;
							break;
						}

					}
					if (add) {
						queue.add(ep);
					}

				}
			}
			boolean add = true;
			// add this entrypoint if it isn't already connected within a tile
			for (EntryPoint e : list) {
				if (e.tile.collide(check.tile)
						&& e.tile.isReachable(e.border, check.border)) {
					add = false;
					break;
				}

			}
			if (add) {
				list.add(check);
			}
		}

		return list.toArray(new EntryPoint[list.size()]);
	}

	/**
	 * Get all borders somehow reachable from a border
	 * 
	 * @param border
	 *            This tile's border that is used as starting point.
	 * @return An array of int containing the borders that are reachable from
	 *         the border.
	 */

	public int[] getReachableBorders(int border) {
		// results go here
		ArrayList<Integer> res = new ArrayList<Integer>();
		// borders to check in the future
		PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
		// add this border
		queue.add(border);
		// borders that we should not check again
		while (!queue.isEmpty()) {
			int check = queue.poll();
			// don't add borders twice
			if (!res.contains(check))
				res.add(check);
			// find out where we can go from this border
			int[] c = getConnections(check);
			for (int i : c) {
				// make sure not to add things that are already checked
				if (!res.contains(i)) {
					queue.add(i);
				}
			}
			// this border is checked and it's connections are added to the
			// queue so we mark this one as checked
		}
		int[] resArr = new int[res.size()];
		for (int i = 0; i < res.size(); ++i) {
			resArr[i] = res.get(i);
		}
		return resArr;
	}

	/**
	 * Gets what tile is at a certain border. This method simplifies it's
	 * argument so BORDER_DIR_NNE, BORDER_DIR_N, BIRDER_DIR_NNW will all return
	 * northBorder and so on.
	 * 
	 * @param border
	 *            The border to query
	 * @return The tile at the specified border or null if no tile is connected
	 *         at this border
	 */

	public Tile getNeighbour(int border) {
		switch (border) {
		case BORDER_DIR_NNE:
		case BORDER_DIR_N:
		case BORDER_DIR_NNW:
			return northTile;
		case BORDER_DIR_ENE:
		case BORDER_DIR_E:
		case BORDER_DIR_ESE:
			return eastTile;
		case BORDER_DIR_SSE:
		case BORDER_DIR_S:
		case BORDER_DIR_SSW:
			return southTile;
		case BORDER_DIR_WSW:
		case BORDER_DIR_W:
		case BORDER_DIR_WNW:
			return westTile;
		default:
			return null;
		}

	}

	/**
	 * Get which of the borders of a neighbouring tile this tile is connected to
	 * in respect of which border you came from. For example: if this tile is
	 * connected to another tile through this tile's north border and the other
	 * tile's west border (which is possible because tiles aren't (always)
	 * concerned about their rotations), connectedVia(BORDER_DIR_N) would return
	 * BORDER_DIR_W and connectedVia(BORDER_DIR_NNW) would return
	 * BORDER_DIR_WSW. Simply put this method returns the neighbouring tile's
	 * border given a connected border on this tile.
	 * 
	 * @param border
	 *            This tile's border to check
	 * @return The neighbouring tile's border that this tile is connected to or
	 *         BORDER_EMPTY if there is no neighbour at the specified border.
	 */

	public int connectedVia(int border) {
		Tile t = getNeighbour(border);
		if (t != null) {
			int i = t.neighboursBorder(this);
			if (border == BORDER_DIR_NNW || border == BORDER_DIR_ENE
					|| border == BORDER_DIR_SSE || border == BORDER_DIR_WSW)
				return --i;
			if (border == BORDER_DIR_NNE || border == BORDER_DIR_ESE
					|| border == BORDER_DIR_SSW || border == BORDER_DIR_WNW)
				return ++i;
			return i;
		} else
			return BORDER_EMPTY;
	}

	/**
	 * Checks if two borders are connected in any way within this tile.
	 * 
	 * @param border1
	 *            The first border to check.
	 * @param border2
	 *            The second border to check.
	 * @return True if the borders are connected within this tile.
	 */
	public boolean isReachable(int border1, int border2) {
		int[] c = getReachableBorders(border1);
		for (int i : c)
			if (i == border2)
				return true;
		return false;
	}

	/**
	 * Get what border a tile is connected to, seen from this tile.
	 * 
	 * @param neighbour
	 *            The neighbour to look for.
	 * 
	 * @return The border that the specified tile is connected to, or
	 *         BORDER_EMPTY if the specified tile is not a neighbour to this
	 *         tile.
	 */

	public int neighboursBorder(Tile neighbour) {
		if (neighbour == northTile)
			return BORDER_DIR_N;
		if (neighbour == westTile)
			return BORDER_DIR_W;
		if (neighbour == southTile)
			return BORDER_DIR_S;
		if (neighbour == eastTile)
			return BORDER_DIR_E;
		return BORDER_EMPTY;
	}

	/**
	 * Connects two tiles by making them neighbours. This method will connect
	 * both tiles in a bidirectional relationship. Does not check if the tile's
	 * borders are the same, but this method can fail if this tile or the
	 * proposed neighbour already has a neighbour at that border.
	 * 
	 * @param t
	 *            The tile to connect to this tile.
	 * @param border
	 *            This tile's border (that will be next to the neighbouring
	 *            tile).
	 * @return True if the tile's were made neighbours. False if they could not
	 *         be.
	 */

	public boolean addNeighbour(Tile t, int border) {
		int ourBorder = accountForRotation(otherSide(border));
		int theirBorder = t.accountForRotation(border);
		if (getNeighbour(ourBorder) != null
				|| t.getNeighbour(theirBorder) != null)
			return false;
		switch (ourBorder) {
		case BORDER_DIR_N:
			northTile = t;
			break;
		case BORDER_DIR_E:
			eastTile = t;
			break;
		case BORDER_DIR_S:
			southTile = t;
			break;
		case BORDER_DIR_W:
			westTile = t;
			break;
		default:
			return false;
		}

		switch (theirBorder) {
		case BORDER_DIR_N:
			t.northTile = this;
			break;
		case BORDER_DIR_E:
			t.eastTile = this;
			break;
		case BORDER_DIR_S:
			t.southTile = this;
			break;
		case BORDER_DIR_W:
			t.westTile = this;
			break;
		default:
			return false;
		}

		return true;

	}

	/**
	 * Connects two borders within a tile in a bidirectional traversable manner.
	 * 
	 * @param border1
	 *            The first border to connect
	 * @param border2
	 *            The second border to connect
	 * @return True if a new connection was made
	 */

	public boolean connect(int border1, int border2) {
		Connection c = null;
		try {
			c = new Connection(border1, border2);
		} catch (IllegalArgumentException e) {
			return false;
		}
		// not allowed to add connections twice
		if (connections.contains(c))
			return false;
		try {
			connections.add(c);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	/**
	 * Fully connects two borders by connecting their bordering borders and then
	 * connecting the borders themselves. This method's arguments should be
	 * north, east, south or west. <br>
	 * Example: <br>
	 * fullyConnect(BORDER_DIR_N, BORDER_DIR_W) will connect N and NNW, N and
	 * NNE, W and WNW, W and WSW, N and W.
	 * 
	 * 
	 * @param border1
	 *            BORDER_N, BORDER_W, BORDER_S or BORDER_E
	 * @param border2
	 *            BORDER_N, BORDER_W, BORDER_S or BORDER_E
	 * @return True if at least one new connection was made
	 */
	public boolean fullyConnect(int border1, int border2) {
		boolean addedSomething = false;
		addedSomething |= connect(border1 - 1, border1);
		addedSomething |= connect(border1 + 1, border1);
		addedSomething |= connect(border2 - 1, border2);
		addedSomething |= connect(border2 + 1, border2);
		addedSomething |= connect(border1, border2);
		return addedSomething;
	}

	/**
	 * Fully connects a side of the tile. For example:
	 * <code>connectSide(BORDER_DIR_N)</code> will connect WNW and NNW, NNW and
	 * N, N and NNE, NNE and ENE. This method's argument should be north, east,
	 * south or west.
	 * 
	 * @param border
	 *            BORDER_DIR_N, BORDER_DIR_E, BORDER_DIR_S or BORDER_DIR_W
	 * @return True if at least one new connection was made
	 */

	public boolean connectSide(int border) {
		if (!isNWSE(border))
			return false;
		boolean addedSomething = false;
		addedSomething |= connect(border - 1, border);
		addedSomething |= connect(border + 1, border);
		// BORDER_DIR_N - 2 is not a valid border, but it should equal
		// BORDER_DIR_ENE
		if (border == BORDER_DIR_N)
			addedSomething |= connect(BORDER_DIR_WNW, border);
		else
			addedSomething |= connect(border - 2, border);

		if (border == BORDER_DIR_W)
			addedSomething |= connect(BORDER_DIR_NNW, border);
		else
			addedSomething |= connect(border + 2, border);
		return addedSomething;
	}

	/**
	 * Gets all borders that are directly connected to this border.
	 * 
	 * @param border
	 *            The border to check for connections
	 * @return An array containing all the borders directly connected to this
	 *         border
	 */

	public int[] getConnections(int border) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for (Connection c : connections) {
			int otherSide = c.otherSide(border);
			if (otherSide != BORDER_EMPTY)
				res.add(otherSide);
		}
		int[] resArr = new int[res.size()];
		for (int i = 0; i < res.size(); ++i) {
			resArr[i] = res.get(i);
		}
		return resArr;
	}

	/**
	 * Returns a string representation of a border.
	 * 
	 * @param border
	 *            The border to turn into a string.
	 * @return A string representing this border.
	 */

	public String borderToString(int border) {
		switch (border) {
		case BORDER_EMPTY:
			return "";
		case BORDER_DIR_NNW:
			return "NNW";
		case BORDER_DIR_N:
			return "N";
		case BORDER_DIR_NNE:
			return "NNE";
		case BORDER_DIR_ENE:
			return "ENE";
		case BORDER_DIR_E:
			return "E";
		case BORDER_DIR_ESE:
			return "ESE";
		case BORDER_DIR_SSE:
			return "SSE";
		case BORDER_DIR_S:
			return "S";
		case BORDER_DIR_SSW:
			return "SSW";
		case BORDER_DIR_WSW:
			return "WSW";
		case BORDER_DIR_W:
			return "W";
		case BORDER_DIR_WNW:
			return "WNW";
		case BORDER_FIELD:
			return "field";
		case BORDER_CITY:
			return "city";
		case BORDER_RIVER:
			return "river";
		case BORDER_ROAD:
			return "road";
		default:
			return "";
		}
	}

	/**
	 * Sets this tile's x and y-coordinates
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 * @param rotation
	 *            The tile's rotation
	 */
	public void setCoords(int x, int y, int rotation) {
		this.x = x;
		this.y = y;
		this.rotation = rotation;
	}

	/**
	 * Check if htis tile has the same x and y-cooridinate as another tile
	 * 
	 * @param t
	 *            The tile to check
	 * @return True if this tile's x and y-cooridnates are the same as the
	 *         specified tile.
	 */

	public boolean collide(Tile t) {
		return x == t.x && y == t.y;
	}

	/**
	 * Check if htis tile has the same x and y-cooridinate as something else.
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 * @return True if this tile's x and y-cooridnates are the same as the ones
	 *         specified.
	 */

	public boolean collide(int x, int y) {
		return this.x == x && this.y == y;
	}

	/**
	 * Get the version of the image that should be used when drawing this tile.
	 * 
	 * @return 0 if this tile's rotation is ROTATION_N, 1 if ROTATION_W, 2 if
	 *         ROTATIONS, 3 if ROTATION_E and -1 otherwise.
	 */

	public int getRotationImage() {
		switch (rotation) {
		case ROTATION_N:
			return 0;
		case ROTATION_W:
			return 1;
		case ROTATION_S:
			return 2;
		case ROTATION_E:
			return 3;
		}
		return -1;
	}

	/**
	 * Get this tiles x and y-coordinates
	 * 
	 * @return An array of int containing { x, y }
	 */

	public int[] getCoords() {
		int[] res = { x, y };
		return res;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tile ");
		sb.append("N ").append(borderToString(northBorder));
		sb.append(" E ").append(borderToString(eastBorder));
		sb.append(" S ").append(borderToString(southBorder));
		sb.append(" W ").append(borderToString(westBorder)).append(" ");
		sb.append(x).append(" ").append(y).append(" ").append(rotation)
				.append(" ");
		for (Connection c : connections) {
			sb.append(borderToString(c.border1)).append("<->")
					.append(borderToString(c.border2)).append(" ");
		}
		return sb.toString();
	}

	/**
	 * Check if this tile contains a river
	 * 
	 * @return True if at least one of the borders of this tile is of type
	 *         BORDER_RIVER
	 */

	public boolean isRiver() {
		return northBorder == BORDER_RIVER || eastBorder == BORDER_RIVER
				|| southBorder == BORDER_RIVER || westBorder == BORDER_RIVER;
	}

	/**
	 * Scores this tile based on a border. If the border is for example part of
	 * a complete city, the number of points that city yields is returned.
	 * 
	 * @param border
	 *            A border that is connected to what should be scored.
	 * @return The amount of points the thing at the border is worth.
	 */

	public int score(int border) {
		if (!isBorder(border))
			return 0;
		EntryPoint[] ep = getAllConnections(border);
		for (EntryPoint e : ep) {
			if (!e.allConnectionsClosed())
				return 0;
		}
		// all entrypoints are assumed to be of the same type
		EntryPoint ep0 = ep[0];
		int borderType = ep0.tile.getBorderType(ep0.border);
		if (borderType == BORDER_CITY) {
			ep = filter(ep);
			int shields = 0;
			for (int i = 0; i < ep.length; ++i) {
				EntryPoint e = ep[i];
				if (e.tile.special == SPECIAL_SHIELD
						&& e.tile.isReachable(e.border, e.tile.specialParent)) {
					shields++;
				}
			}
			return 2 * (ep.length + shields);
		} else if (borderType == BORDER_ROAD) {
			return ep.length;
		}
		return 0;

	}

	/**
	 * Removes duplicate tiles.
	 * 
	 * @param ep
	 *            An array of EntryPoint.
	 * @return An array of EntryPoint that has no duplicate tiles.
	 */

	private EntryPoint[] filter(EntryPoint[] ep) {
		ArrayList<EntryPoint> res = new ArrayList<EntryPoint>();
		for (int i = 0; i < ep.length; ++i) {
			boolean add = true;
			EntryPoint e = ep[i];
			for (int j = 0; j < res.size(); ++j) {
				if (res.get(j).collide(e)) {
					add = false;
					break;
				}
			}
			if (add)
				res.add(e);
		}
		return res.toArray(new EntryPoint[res.size()]);
	}

	/**
	 * A connection is a bidirectional connection that connects two borders
	 * within a single tile.
	 *
	 */

	private class Connection {

		protected int border1;
		protected int border2;

		/**
		 * Creates a new connection
		 * 
		 * @param border1
		 *            The first border
		 * @param border2
		 *            The second border
		 * @throws IllegalArgumentException
		 *             If the specified borders are not borders or if they are
		 *             the same border.
		 */

		public Connection(int border1, int border2)
				throws IllegalArgumentException {

			if (isBorder(border1) && isBorder(border2) && border1 != border2) {
				this.border1 = border1;
				this.border2 = border2;
			} else
				throw new IllegalArgumentException();

		}

		/**
		 * Check if two connections are equal. Since connections are
		 * bidirectional, check if c.border1 == border2 as well
		 * 
		 * @param c
		 *            The connection to check for equality.
		 * @return True if this connection is equal to the parameter c.
		 */
		public boolean equals(Object o) {
			Connection c = (Connection) o;
			return c.border1 == border1 && c.border2 == border2
					|| c.border1 == border2 && c.border2 == border1;
		}

		/**
		 * Get the other side of this connection.
		 * 
		 * @param border
		 *            One of this connections borders.
		 * @return The other border in this connection, or BORDER_EMPTY if
		 *         border is not part of this connection.
		 */

		public int otherSide(int border) {
			if (border == border1)
				return border2;
			else if (border == border2)
				return border1;
			else
				return BORDER_EMPTY;

		}

	}

}
