package game;

import game.Tile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class TileScanner {
	private Scanner scanner;
	private ArrayList<Tile> tiles;
	private ArrayList<Integer> ids;

	/**
	 * Creates a TileScanner that can be used to scan a String for tiles.
	 * 
	 * @param source
	 *            The source containing the String versions of the tiles.
	 */

	public TileScanner(String source) {
		scanner = new Scanner(source);
		tiles = new ArrayList<Tile>();
		ids = new ArrayList<Integer>();

	}

	/**
	 * Creates a TileScanner that can be used to scan an InputStream for tiles.
	 * 
	 * @param source
	 *            The source containing the String versions of the tiles.
	 */

	public TileScanner(InputStream source) {
		scanner = new Scanner(source);
		tiles = new ArrayList<Tile>();
		ids = new ArrayList<Integer>();
	}

	/**
	 * Creates a TileScanner that can be used to scan a BufferedReader for
	 * tiles.
	 * 
	 * @param source
	 *            The source containing the String versions of the tiles.
	 */
	public TileScanner(BufferedReader source) {
		scanner = new Scanner(source);
		tiles = new ArrayList<Tile>();
		ids = new ArrayList<Integer>();
	}

	/**
	 * Scan the source for one tile. Tiles in the source should be on the format
	 * <code>Tile id northBorder eastBorder southBorder westBorder special
	 * specialBorder image [connections] </code><br>
	 * The id should be an int. No duplicate ids are allowed.<br>
	 * The image should be an int that describes which row of 50 pixels that
	 * describes this tile in the file "Tile.png"<br>
	 * Connections should be on the format <code>type border1 [border2] </code> <br>
	 * where type is <code>c</code> for normal connection, <code>side</code> for
	 * a side connection (5 adjacent borders) or <code>full</code> for a full
	 * connection (2 sides of 3 borders all connected). The second border should
	 * only be used if type is not side. <br>
	 * Neighbours should be on the format
	 * <code>Neighbour id1 direction id2 </code><br>
	 * 
	 * 
	 * 
	 * @return Returns a Tile representing the tile that was read from the
	 *         source.
	 */

	public Tile[] scan() {
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			String[] arr = line.split(" ");
			if (arr[0].equals("Tile"))
				scanTile(arr);
			else if (arr[0].equals("Neighbour"))
				scanNeighbour(arr);
		}
		return tiles.toArray(new Tile[tiles.size()]);
	}

	private void scanNeighbour(String[] arr) {
		Tile t1, t2;
		int id1 = Integer.parseInt(arr[1]);
		int id2 = Integer.parseInt(arr[3]);
		int index1 = -1;
		int index2 = -1;
		for (int i = 0; i < ids.size(); ++i) {
			int id = ids.get(i);
			if (id == id1)
				index1 = i;
			else if (id == id2)
				index2 = i;
		}
		if (index1 != -1 && index2 != -1) {
			t1 = tiles.get(index1);
			t2 = tiles.get(index2);
		} else {
			return;
		}
		t1.addNeighbour(t2, stringToBorder(arr[2]));

	}

	public void scanTile(String[] arr) {
		try {
			Tile t = new Tile(stringToBorder(arr[2]), stringToBorder(arr[3]),
					stringToBorder(arr[4]), stringToBorder(arr[5]),
					stringToBorder(arr[6]), stringToBorder(arr[7]));
			t.setImage(Integer.parseInt(arr[8]));
			// add the connections
			for (int i = 9; i < arr.length; ++i) {
				switch (arr[i]) {
				case "c":
					t.connect(stringToBorder(arr[++i]),
							stringToBorder(arr[++i]));
					break;
				case "side":
					t.connectSide(stringToBorder(arr[++i]));
					break;
				case "full":
					t.fullyConnect(stringToBorder(arr[++i]),
							stringToBorder(arr[++i]));
					break;
				default:
					return;
				}
			}
			ids.add(Integer.parseInt(arr[1]));
			tiles.add(t);
			return;

		} catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
			// return in case of bad input
			return;
		}

	}

	private int stringToBorder(String border) {
		switch (border) {
		case "N":
			return Tile.BORDER_DIR_N;
		case "NNW":
			return Tile.BORDER_DIR_NNW;
		case "NNE":
			return Tile.BORDER_DIR_NNE;
		case "W":
			return Tile.BORDER_DIR_W;
		case "WSW":
			return Tile.BORDER_DIR_WSW;
		case "WNW":
			return Tile.BORDER_DIR_WNW;
		case "S":
			return Tile.BORDER_DIR_S;
		case "SSW":
			return Tile.BORDER_DIR_SSW;
		case "SSE":
			return Tile.BORDER_DIR_SSE;
		case "E":
			return Tile.BORDER_DIR_E;
		case "ESE":
			return Tile.BORDER_DIR_ESE;
		case "ENE":
			return Tile.BORDER_DIR_ENE;
		case "CITY":
			return Tile.BORDER_CITY;
		case "ROAD":
			return Tile.BORDER_ROAD;
		case "FIELD":
			return Tile.BORDER_FIELD;
		case "RIVER":
			return Tile.BORDER_RIVER;
		case "EMPTY":
			return Tile.BORDER_EMPTY;
		default:
			return Tile.BORDER_EMPTY;
		}
	}

}
