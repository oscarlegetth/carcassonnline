package game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TestSomeTiles {

	public static Tile[] someTiles() {
		TileScanner ts = null;
		try {
			ts = new TileScanner(new BufferedReader(new FileReader("test1")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ts.scan();
	}

	public static Tile[] someQueueTiles() {
		TileScanner ts = null;
		try {
			ts = new TileScanner(new BufferedReader(new FileReader("testqueue")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ts.scan();
	}

	public static Tile[] riverStart() {
		TileScanner ts = new TileScanner("Tile 1 FIELD RIVER FIELD FIELD EMPTY EMPTY 2 side N side W side S");
		return ts.scan();
	}

	public static Tile[] riverTiles() {
		TileScanner ts = null;
		try {
			ts = new TileScanner(new BufferedReader(new FileReader("RiverTiles")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ts.scan();
	}

	public static Tile[] riverEnd() {
		TileScanner ts = new TileScanner("Tile 1 FIELD RIVER FIELD FIELD EMPTY EMPTY 8 side N side W side S");
		return ts.scan();
	}

	// public static void main(String[] args) {
	// String tiles = "Tile 1 CITY ROAD ROAD CITY EMPTY EMPTY full N W c ENE SSW
	// c S E c SSE ESE\nTile 2 CITY FIELD FIELD CITY EMPTY EMPTY full N W full S
	// E\nNeighbour 1 N 2 N\n";
	// TileScanner ts = new TileScanner(tiles);
	// Tile[] t = ts.scan();
	// for (Tile s : t)
	// System.out.println(s);
	//
	// }

}
