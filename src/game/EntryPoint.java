package game;

/**
 * An EntryPoint is a tile and a border. Used for remembering places we've been
 * at already
 *
 */

class EntryPoint {
	int border;
	Tile tile;

	public EntryPoint(Tile t, int border) {
		this.tile = t;
		this.border = border;
	}

	public String toString() {
		return tile + " " + border;
	}

	public boolean allConnectionsClosed() {
		int[] borders = tile.getReachableBorders(border);
		for (int b : borders) {
			if (tile.connectedVia(b) == Tile.BORDER_EMPTY)
				return false;
		}
		return true;
	}

	public boolean collide(EntryPoint entryPoint) {
		return tile.collide(entryPoint.tile);
	}
}