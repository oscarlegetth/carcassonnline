package client;

import game.GameHandler;
import game.Tile;
import gui.GUI;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientHandler implements game.Handler {

	private Socket socket;
	private MessageHandler messageHandler;
	private GUI gui;
	private GameHandler gameHandler;

	public ClientHandler() {

		Scanner scanner = new Scanner(System.in);
		// { IP , port }
		String serverIPAndPort[];
		String serverIP = "";
		int serverPort = 0;
		while (true) {
			System.out.flush();
			System.out.print("Enter a server's IP and port ");
			serverIPAndPort = scanner.nextLine().split(" ");
			if (serverIPAndPort.length == 2) {
				serverIP = serverIPAndPort[0];
				try {
					serverPort = Integer.parseInt(serverIPAndPort[1]);
				} catch (NumberFormatException e) {
					System.err.println("Port is not an int");
					continue;
				}
				try {
					System.out.println(serverIP + " " + serverPort);
					socket = new Socket(serverIP, serverPort);
					break;
				} catch (IOException e) {
					e.printStackTrace();
					System.err.println("Server not found");
				}
			}
		}
		scanner.close();
		InputStream in = null;
		try {
			in = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Server says: " + readLine(in));
		messageHandler = new MessageHandler(socket, this);
		gameHandler = new GameHandler(this);
		gui = new GUI(gameHandler);
		gameHandler.setGUI(gui);
	}

	public String readLine(InputStream in) {
		String res = "";
		try {
			while (in.available() > 0) {
				res += in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static void main(String[] args) {
		new ClientHandler();
	}

	public void addTile(Tile t) {

	}
}
