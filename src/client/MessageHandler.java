package client;

import game.Tile;
import game.TileScanner;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import server.Protocol;

public class MessageHandler extends Thread {

	private OutputStream os;
	private InputStream is;
	private ClientHandler client;

	public MessageHandler(Socket s, ClientHandler client) {
		try {
			os = s.getOutputStream();
			is = s.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.client = client;
	}

	public void run() {
		while (!isInterrupted())
			try {
				while (is.available() > 0) {
					byte message = (byte) is.read();
					if (message != -1) {
						handleMessage(message);
					} else {
						throw new IOException();
					}
				}
				wait();

			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
	}

	private void handleMessage(byte message) throws IOException {
		switch (message) {
		case Protocol.ACTION_SEND_TILE:
			int bytesLeft = is.read();
			int off = bytesLeft;
			byte[] data = new byte[bytesLeft];
			while (bytesLeft > 0) {
				int bytesRead = is.read(data, off, bytesLeft);
				bytesLeft -= bytesRead;
				off += bytesRead;
			}
			String s = new String(data);
			client.addTile(new TileScanner(s).scan()[0]);
			break;
		}
	}
}
