package server;

import game.GameHandler;
import game.Tile;
import gui.GUI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

public class ServerHandler extends Thread implements game.Handler {

	private ServerSocket serverSocket;
	private GameHandler gameHandler;
	private GUI serverGUI;
	public Socket[] clients;

	public ServerHandler() {

		try {
			// Initialize a socket for clients to connect to. 0 as argument
			// gives us a free port
			serverSocket = new ServerSocket(55720);
			// serverSocket.setSoTimeout(10000);
		} catch (IOException e) {
			System.err.println("Port already in use");
		}
		gameHandler = new GameHandler(this);
		serverGUI = new GUI(gameHandler);
		gameHandler.setGUI(serverGUI);

		// serverGUI.repaint();

		System.out.println("Server started at " + whatIsMyIP() + " "
				+ serverSocket.getLocalPort());

		clients = new Socket[4];
		int clientn = 0;

		try {
			while (!gameHandler.gameOver()) {
				// Wait for plebs to connect
				System.out.println("Waiting...");
				clients[clientn] = serverSocket.accept();
				// clients[clientn].getOutputStream().write(toByteArray(1));
				clientn++;
				System.out.println("Client connected");
			}
		} catch (IOException e) {
			System.err.println("Socket time out or something");
		}

		try {
			// gg
			serverSocket.close();
		} catch (IOException e) {
			System.err
					.println("Couldn't close the server, the server will now close.");
		}

	}

	private byte[] toByteArray(int data) {
		byte[] result = { (byte) (data >>> 24), (byte) (data >>> 16),
				(byte) (data >>> 8), (byte) data };
		return result;
	}

	/**
	 * Awfully complicated. Thanks stack overflow.
	 * 
	 * @return this server's external IP address
	 */
	private String whatIsMyIP() {
		URL url = null;
		BufferedReader in = null;
		String ipAddress = "";
		try {
			url = new URL("http://bot.whatismyipaddress.com");
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			ipAddress = in.readLine().trim();
			/*
			 * IF not connected to internet, then the above code will return one
			 * empty String, we can check it's length and if length is not
			 * greater than zero, then we can go for LAN IP or Local IP or
			 * PRIVATE IP
			 */
			if (!(ipAddress.length() > 0)) {
				try {
					InetAddress ip = InetAddress.getLocalHost();
					System.out.println((ip.getHostAddress()).trim());
					ipAddress = (ip.getHostAddress()).trim();
				} catch (Exception exp) {
					ipAddress = "ERROR";
				}
			}
		} catch (Exception ex) {
			// This try will give the Private IP of the Host.
			try {
				InetAddress ip = InetAddress.getLocalHost();
				System.out.println((ip.getHostAddress()).trim());
				ipAddress = (ip.getHostAddress()).trim();
			} catch (Exception exp) {
				ipAddress = "ERROR";
			}
			// ex.printStackTrace();
		}
		return ipAddress;
	}

	public static void main(String[] args) {
		Thread t = new ServerHandler();
		t.start();

	}

	public void addTile(Tile t) {
		sendToAll(Protocol.ACTION_PLAY_TILE, Protocol.tileToByteArray(t));
	}

	private void sendToAll(byte message, byte[] data) {
		for (Socket s : clients) {
			if (s != null) {
				try {
					s.getOutputStream().write(message);
					s.getOutputStream().write(data.length);
					s.getOutputStream().write(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
