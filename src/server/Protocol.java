package server;

import game.Tile;

public class Protocol {

	public static final byte ACTION_DRAW_TILE = 1;
	public static final byte ACTION_PLAY_TILE = 2;
	public static final byte ACTION_SEND_TILE = 3;

	public static final byte CONNECTION_PLAYER_CONNECT = 10;
	public static final byte CONNECTION_PLAYER_DISCONNENCT = 11;

	public static byte[] tileToByteArray(Tile t) {
		String s = t.toString();
		byte[] res = new byte[s.length()];
		char[] c = s.toCharArray();
		for (int i = 0; i < c.length; ++i) {
			res[i] = (byte) c[i];
		}
		return res;
	}

}
