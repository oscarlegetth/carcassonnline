package gui;

import game.GameHandler;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ButtonPanel extends JPanel {

	private GameHandler gh;

	public ButtonPanel(final GameHandler gh) {
		super();
		this.gh = gh;

		JButton rotateRightButton = new JButton("<-");
		rotateRightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gh.rotateLeft();
			}
		});

		add(rotateRightButton);
		JButton rotateLeftButton = new JButton("->");
		rotateLeftButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gh.rotateRight();
			}
		});
		add(rotateLeftButton);

		JButton placeTileButton = new JButton("Place tile");
		placeTileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gh.addTile();

			}
		});
		add(placeTileButton);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawString("Scores", 25, 130);

		String[] players = gh.getPlayers();
		int[] scores = gh.getScores();
		for (int i = 0; i < players.length; ++i) {
			g.drawString(players[i] + ": " + scores[i], 25, 150 + i * 20);
		}
	}

}
