package gui;

import game.GameHandler;
import game.Tile;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class NextTilePanel extends JPanel {

	private BufferedImage image;

	private GameHandler gameHandler;

	public NextTilePanel(GameHandler gh) {
		super();
		this.gameHandler = gh;
		try {
			image = ImageIO.read(new File("Tile.png"));
		} catch (IOException e) {
			System.err.print("Can't find some image some tile");
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Tile nextTile = gameHandler.nextTile();
		int rotation = gameHandler.nextTileRotationImage();
		g.drawString("Next tile goes here", 25, 25);
		g.drawString("rotation: " + rotation, 25, 40);
		if (nextTile == null) {
			g.drawString("No more tiles", 50, 50);
		} else {
			Image subImage = image.getSubimage(50 * rotation, 50 * nextTile.getImage(), 50, 50);

			g.drawImage(subImage, 50, 50, null);
		}
	}

}
