package gui;

import game.GameHandler;
import game.Tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.print.attribute.SetOfIntegerSyntax;
import javax.swing.JPanel;

public class GameBoardPanel extends JPanel {

	private BufferedImage image;

	private GameHandler gh;
	private int camx;
	private int camy;
	private int markedTileX;
	private int markedTileY;

	private Tile[] gameTileCache;
	private Tile[] adjacentSquaresCache;

	/**
	 * Creates a GameBoardPanel that a game board may be drawn in.
	 * 
	 * @param gh
	 *            The GameHandler handling the game.
	 */
	public GameBoardPanel(GameHandler gh) {
		super();
		this.gh = gh;
		try {
			image = ImageIO.read(new File("Tile.png"));
		} catch (IOException e) {
			System.err.print("Can't find some image some tile");
		}
		MouseListeners ml = new MouseListeners();
		addMouseListener(ml);
		addMouseMotionListener(ml);
	}

	/**
	 * Paint this component. Draws the tiles in play, the adjacent squares of
	 * tiles in play and the currently marked square.
	 */
	public void paintComponent(Graphics g) {
		// clear whatever is already drawn
		super.paintComponent(g);
		// if something has changed, get all the tiles in play and place them in
		// the cache
		if (gh.hasChanged() || gameTileCache == null) {
			gameTileCache = gh.tiles();
			adjacentSquaresCache = gh.adjacentSquares();
		}
		// draw each tile
		for (int i = 0; i < gameTileCache.length; ++i) {
			Tile t = gameTileCache[i];
			int[] coords = gameTileCache[i].getCoords();
			// Since a tile's image contains all it's rotations, we have to
			// figure out which part of the image to draw
			Image subImage = image.getSubimage(50 * t.getRotationImage(), 50 * t.getImage(), 50, 50);
			g.drawImage(subImage, camx + 50 * coords[0], camy + 50 * coords[1], null);
		}

		for (int i = 0; i < adjacentSquaresCache.length; ++i) {
			int[] coords = adjacentSquaresCache[i].getCoords();
			g.setColor(Color.GRAY);
			g.drawRect(camx + 50 * coords[0], camy + 50 * coords[1], 49, 49);
		}

		g.setColor(Color.CYAN);
		g.drawRect(markedTileX * 50 + camx, markedTileY * 50 + camy, 49, 49);

	}

	public int[] markedTileCoords() {
		int res[] = { markedTileX, markedTileY };
		return res;
	}

	private class MouseListeners extends MouseAdapter {

		private int mousex; // used to figure out how much the camera has moved
							// since the last event
		private int mousey;

		public void mousePressed(MouseEvent me) {
			mousex = me.getX();
			mousey = me.getY();

		}

		public void mouseDragged(MouseEvent me) {
			camx += (me.getX() - mousex);
			camy += (me.getY() - mousey);
			mousex = me.getX();
			mousey = me.getY();
			// System.out.println(camx + " " + camy);
			repaint();

		}

		public void mouseClicked(MouseEvent me) {
			// hard core math
			int x = (me.getX() - camx % 50 - (camx / 50 * 50));
			int y = (me.getY() - camy % 50 - (camy / 50 * 50));
			if (x < 0)
				x -= 50;
			if (y < 0)
				y -= 50;
			markedTileX = x / 50;
			markedTileY = y / 50;
			// TODO fix performance ?
			repaint();
		}
	}

}
