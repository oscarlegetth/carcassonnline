package gui;

import game.GameHandler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {

	private GameBoardPanel drawArea;
	private NextTilePanel nextTileArea;
	private ButtonPanel buttonArea;

	private void addPanel(JPanel panel, GridBagLayout gridbag, GridBagConstraints c) {
		gridbag.setConstraints(panel, c);
		add(panel);
	}

	public GUI(GameHandler gh) {

		super("Server GUI");
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		setLayout(gridbag);

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = GridBagConstraints.REMAINDER;
		c.gridwidth = 3;
		c.weightx = 0.0;
		c.weighty = 0.0;
		drawArea = new GameBoardPanel(gh);
		drawArea.setBackground(Color.WHITE);
		drawArea.setPreferredSize(new Dimension(600, 600));
		addPanel(drawArea, gridbag, c);

		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = GridBagConstraints.RELATIVE;
		c.gridheight = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		nextTileArea = new NextTilePanel(gh);
		// nextTileArea.setBackground(Color.GREEN);
		nextTileArea.setPreferredSize(new Dimension(300, 200));
		addPanel(nextTileArea, gridbag, c);

		c.gridheight = GridBagConstraints.REMAINDER;
		buttonArea = new ButtonPanel(gh);
		// buttonArea.setBackground(Color.BLUE);
		buttonArea.setPreferredSize(new Dimension(300, 400));
		addPanel(buttonArea, gridbag, c);
		// setSize(900, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setVisible(true);

	}

	public int[] markedTileCoords() {
		return drawArea.markedTileCoords();
	}

}
